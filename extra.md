<!--
SPDX-FileCopyrightText: © 2020 Carmen Bianca Bakker <carmen@carmenbianca.eu>
SPDX-FileCopyrightText: 2020 Kristian Serrano
SPDX-FileCopyrightText: 2000-2003, 2016 Wizards of the Coast, Inc.

SPDX-License-Identifier: CC-BY-4.0
-->

\onecolumn

# Credits {-}

**Written & designed by** Carmen Bianca Bakker

**Inspired by**
[_Eberron for Savage Worlds_](https://immaterialplane.com/products/eberron-for-savage-worlds/)
by Kristian Serrano

**Eberron created by** Keith Baker and others

**Savage Worlds written & designed by** Shane Lacy Hensley, with Clint Black

_Eberron for Savage Worlds: Extra_ is unofficial Fan Content permitted under the
[Fan Content Policy](https://company.wizards.com/fancontentpolicy). Not
approved/endorsed by Wizards. Portions of the materials used are property of
Wizards of the Coast. ©Wizards of the Coast LLC.

Dungeons & Dragons, D&D, Eberron and its respective logos are trademarks of
Wizards of the Coast in the USA and other countries.

This game references the _Savage Worlds_ game system, available from Pinnacle
Entertainment Group at <https://www.peginc.com>. Savage Worlds and all
associated logos and trademarks are copyrights of Pinnacle Entertainment Group.

Portions of this work are copyright Kristian Serrano, and used with permission
under the condition that the author be attributed. See the hyperlink to _Eberron
for Savage Worlds_ above.

All parts of this work over which the author holds copyright are licensed under
a
[Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/).
© 2023 Carmen Bianca Bakker

![](SWFan.jpg){width=300}

\twocolumn

# Foreword

This is a simple small document with some additional _stuff_ for Kristian
Serrano's
[_Eberron for Savage Worlds_](https://immaterialplane.com/products/eberron-for-savage-worlds/).
It's not really intended for publishing, but congratulations if you somehow
ended up here.

## Source and sharing

The source code for this document is available at
<https://gitlab.com/carmenbianca/eberron-for-savage-worlds-extra>. It is written
in a single Markdown file, and should be really easy to fork and change. Pandoc
is used to generate all file formats.

You can find hosted versions of this document at:

- HTML --- <https://carmenbianca.gitlab.io/eberron-for-savage-worlds-extra/>
- EPUB ---
  [https://carmenbianca.gitlab.io/eberron-for-savage-worlds-extra/Eberron for Savage Worlds: Extra.epub](https://carmenbianca.gitlab.io/eberron-for-savage-worlds-extra/Eberron%20for%20Savage%20Worlds:%20Extra.epub)
- PDF ---
  [https://carmenbianca.gitlab.io/eberron-for-savage-worlds-extra/Eberron for Savage Worlds: Extra.pdf](https://carmenbianca.gitlab.io/eberron-for-savage-worlds-extra/Eberron%20for%20Savage%20Worlds:%20Extra.pdf)

# Ancestries

## Aarakocra

- **Talons (+1):** An aarakocra's claws are **Natural Weapons** (see _Savage
  Worlds_) that cause Strength+d4 damage.
- **Flight (+4):** Aarakocra fly at Pace 12. Their running die for flying is a
  d6.
- **Reduced Pace (-1):** Decrease the character's Pace by 1 and their running
  die one die type.

## Bugbear

<!--
Purely physiological traits copied from 5e. Also stealth.
-->

- **Big (-2):** Bugbears subtract two when using equipment designed for smaller
  beings, and cannot wear humanoid armor or clothing. Equipment, armor, food and
  clothing cost double the listed price.
- **Darkvision (+1):** Bugbears can see in Pitch Darkness up to 10" (20 yards),
  ignoring all Illumination penalties and up to 2 points of penalties from
  invisibility or similar powers.
- **Reach (+1):** Bugbears have long arms. This grants them Reach +1.
- **Size +1 (+1):** Bugbears are tall creatures, increasing their Size (and
  therefore Toughness) by 1.
- **Sneaky (+1)**: Bugbears start with a d6 in Stealth instead of a d4. This
  increases maximum Stealth to d12 + 1.
- **Powerful Build (+2):** Bugbears start with a d6 in Strength instead of a d4.
  This increases maximum Strength to d12 + 1.

## Bullywug

- **Aquatic (+2):** Bullywugs can breathe underwater and move their full Pace
  when swimming.
- **Leaper (+2):** Bullywugs can jump twice as far as listed under **Movement**
  (see _Savage Worlds_). In addition, bullywugs add +4 to damage when leaping as
  part of a Wild Attack instead of the usual +2 (unless in a closed or confined
  space where they cannot leap horizontally or vertically---GM's call).
- **Speak with Frogs and Toads (+2):** Bullywugs can communicate with frogs and
  toads. They have the Beast Talker Edge.
- **Water Dependency (-2):** Bullywugs must immerse themselves in water one hour
  out of every 24 or become automatically Fatigued each day until they are
  Incapacitated. The day after Incapacitation from dehydration, they perish.
  Each hour spent in water restores one level of Fatigue.

## Dragonborn

<!-- Almost-verbatim copy from 5e. -->

- **Breath Weapon (+2):** Dragonborn can breathe destructive energy by making an
  Athletics roll as a limited action. This uses the Cone Template, may be
  Evaded, and causes 2d6 damage (3d6 with a raise on the Athletics roll). A
  Critical Failure on the attack causes Fatigue. Their ancestry's energy type
  determines an additional effect:
  - **Acid:** Anyone Shaken or Wounded by the breath weapon automatically takes
    2d4 damage at the start of their next turn. If the target is wearing armor,
    roll a d6. On a 5 or 6 the armor loses one point of protection.
  - **Cold:** If Shaken or Wounded by the attack, the target's Pace is reduced
    by 2 for five rounds. These effects are cumulative to a minimum Pace of 1.
  - **Fire:** The breath weapon causes +2 damage and may set the target on fire.
  - **Lightning:** Anyone Shaken or Wounded by the attack must make a Vigor roll
    or be Stunned.
- **Environmental Resistance (+1):** Dragonborn receive a +4 bonus to resist a
  negative environmental effect determined by their draconic ancestry, such as
  heat, cold, acid, poison, or lightning. Damage from that source is also
  reduced by 4.
- **Size +1 (+1):** Dragonborn are tall creatures, increasing their Size (and
  therefore Toughness) by 1.

## Drow

<!-- Almost identical to elves. Added Sunlight Sensitivity and Magic
Resistance. -->

- **Agile (+2):** Drow are graceful and agile. They start with a d6 in Agility
  instead of a d4. This increases maximum Agility to d12+1.
- **Darkvision (+1):** Drow can see in Pitch Darkness up to 10" (20 yards),
  ignoring all Illumination penalties and up to 2 points of penalties from
  invisibility or similar powers.
- **Magic Resistance (+2):** Drow have tremendous resistance to magic. They have
  the Arcane Resistance Edge.
- **Frail (-1):** Drow aren't as sturdy as most ancestries. Their Toughness is
  reduced by 1.
- **Keen Vision (+1):** Drow have a +2 to Notice rolls to detect obscured or
  hidden features.
- **Sunlight Sensitivity (-2):** Drow are Distracted when exposed to sunlight.
- **Trance (+1):** Drow don't need to sleep, but they must meditate in a
  semiconscious state 4 hours every day.

## Genasi

- **Elemental Dependency (-2):** Genasi weaken if they spend too long away from
  their element. They need to spend at least one hour out of every 24 in close
  proximity to their element. They take a level of Fatigue each day if they
  can’t. Each hour spent near the element restores one level of Fatigue. This
  cannot lead to Incapacitation.
- **Elemental Manipulation (+2):** Genasi have the Arcane Background (Gifted)
  Edge, and know the _elemental manipulation_ power. The power has a Limitation
  such that it can only be used for their ancestral element.
- **Elemental Resistance (+1):** Genasi receive a +4 bonus to resist negative
  effects from their ancestral element, and damage from that element is also
  reduced by 4.

### Genasi, Air

- **Levitate (+1):** Air genasi can levitate close to the ground and never take
  damage from falling.
- **Unending Breath (+2):** Air genasi can hold their breath indefinitely.

### Genasi, Earth

- **Darkvision (+1):** Earth genasi ignore penalties for Illumination up to 10"
  (20 yards).
- **Earth Walk (+1):** Earth genasi ignore penalties for Difficult Ground made
  of earth or stone. They can pass over the ground without leaving a trace.
- **Toughness +1 (+1):** The element of earth grants earth genasi extra
  resilience. They have Toughness +1.

### Genasi, Fire

- **Fire Focus (+2):** Fire genasi can create a burst of fire by making an
  Athletics roll as a limited action. This uses the Cone Template, may be
  Evaded, and causes 2d6 damage (3d6 with a raise on the Athletics roll). This
  has a chance to set someone on fire (see **Hazards** in the _Savage Worlds_).
  A Critical Failure on the attack causes Fatigue.
- **Infravision (+1):** Fire genasi can see in the infrared spectrum, halving
  penalties for bad lighting when attacking targets that radiate heat.

### Genasi, Water

- **Aquatic (+2):** Water genasi can breathe underwater and move their full Pace
  when swimming.
- **Low Light Vision (+1):** Water genasi ignore penalties for Dim and Dark
  Illumination.

## Gnoll

- **Bite (+1):** A gnolls's fangs are **Natural Weapons** (see _Savage Worlds_)
  that cause Strength+d4 damage.
- **Darkvision (+1):** Gnolls can see in Pitch Darkness up to 10" (20 yards),
  ignoring all Illumination penalties and up to 2 points of penalties from
  invisibility or similar powers.
- **Pack Tactics (+1):** Gnolls add any Gang Up bonus to their Fighting damage
  rolls.
- **Size +1 (+1):** The average gnoll stands taller than 7', increasing their
  Size (and therefore Toughness) by 1.

## Goblin

<!-- Goblins are small and have darkvision. -->

- **Agile (+2):** Goblins start with a d6 in Agility instead of a d4. This
  increases maximum Agility to d12 + 1.
- **Elusive (+1):** Goblins start with a d6 in Stealth instead of a d4. This
  increases maximum Stealth to d12 + 1.
- **Darkvision (+1):** Goblins can see in Pitch Darkness up to 10" (20 yards),
  ignoring all Illumination penalties and up to 2 points of penalties from
  invisibility or similar powers.
- **Nimble (+1):** Goblins start with a d4 in Thievery.
- **Size -1 (-1):** Goblins stand three to four feet tall, reducing their Size
  (and therefore Toughness) by 1.

## Goliath

- **Armor +2 (+1):** Goliath skin is especially tough, granting them Armor +2.
- **Powerful Build (+2):** Goliaths start with a d6 in Strength instead of a d4.
  This increases maximum Strength to d12 + 1.
- **Size +1 (+1):** Goliaths are 7--8 feet of muscle, increasing their Size (and
  therefore Toughness) by 1.

## Grung

- **Amphibious (+1):** Grungs can breathe underwater.
- **Natural Climber (+1):** Grungs can climb vertical surfaces as though walking
  normally, or inverted surfaces at half Pace.
- **Poison Immunity (+1):** Grungs are immune to poison.
- **Poisonous Skin (+3):** With a successful regular or Touch Attack, the victim
  must roll Vigor or suffer the effects of Mild Poison. They also suffer an
  additional effect for 2d4 rounds depending on the grung's skin color.
  - **Blue:** The victim must shout loudly or otherwise make a loud noise as a
    free action on their turn.
  - **Gold:** The victim regards the grung as a friend and can speak Grung. The
    grung gains +1 to Intimidation, Persuasion, Performance, or Taunt rolls
    against the victim.
  - **Green:** The victim can't move except to climb or jump.
  - **Orange:** The victim is frightened of their allies. While in the presence
    of their allies, they are Distracted.
  - **Purple:** The victim feels a desperate need to soak themself in liquid or
    mud. They are Distracted unless immersed in a body of liquid or mud.
  - **Red:** The victim is incredibly hungry. They must use at least one action
    each turn to eat. If they do not eat, they are Distracted.
- **Reduced Pace (-1):** Decrease the character's Pace by 1 and their running
  die one die type.
- **Size -1 (-1):** Grungs average about three feet tall, reducing their Size
  (and therefore Toughness) by 1.
- **Leaper (+2):** Grungs can jump twice as far as listed under **Movement**
  (see _Savage Worlds_). In addition, grungs add +4 to damage when leaping as
  part of a Wild Attack instead of the usual +2 (unless in a closed or confined
  space where they cannot leap horizontally or vertically---GM's call).
- **Water Dependency (-2):** Grungs must immerse themselves in water one hour
  out of every 24 or become automatically Fatigued each day until they are
  Incapacitated. The day after Incapacitation from dehydration, they perish.
  Each hour spent in water restores one level of Fatigue.

## Hobgoblin

<!-- I hate this race. -->

- **Darkvision (+1):** Hobgoblins can see in Pitch Darkness up to 10" (20
  yards), ignoring all Illumination penalties and up to 2 points of penalties
  from invisibility or similar powers.
- **Jingoistic (-1):** Hobgoblins have the Jingoistic (Minor) Hindrance.
- **Spirited (+2):** Hobgoblins start with a d6 in Spirit instead of a d4. This
  increases maximum Spirit to d12 + 1.
- **Unshakeable (+2):** Hobgoblins do not easily flinch. They begin play with
  the Combat Reflexes or the Command Edge---their choice.

## Koalinth

<!-- I also hate this race. -->

- **Aquatic (+2):** Koalinth can breathe underwater and move their full Pace
  when swimming.
- **Darkvision (+1):** Koalinth can see in Pitch Darkness up to 10" (20 yards),
  ignoring all Illumination penalties and up to 2 points of penalties from
  invisibility or similar powers.
- **Jingoistic (-1):** Koalinth have the Jingoistic (Minor) Hindrance.
- **Unshakeable (+2):** Koalinth are the hobgoblins of the sea. They begin play
  with the Combat Reflexes or the Command Edge---their choice.

## Kobold

<!-- TODO -->

- **Armor +2 (+1):** Kobolds have tough, scaly skin, increasing their Armor
  by 2.
- **Agile (+2):** Kobolds start with a d6 in Agility instead of a d4. This
  increases maximum Agility to d12 + 1
- **Darkvision (+1):** Kobolds ignore penalties for Illumination up to 10" (20
  yards).
- **Pack Tactics (+1):** Kobolds add any Gang Up bonus to their Fighting damage
  rolls.
- **Size -1 (-1):** Kobolds are between two and three feet, reducing their Size
  (and therefore Toughness) by 1.
- **Sunlight Sensitivity (-2):** Kobolds are Distracted when exposed to
  sunlight.

> #### Variant: Winged Kobolds
>
> Replace Agile or Pack Tactics with the following feature:
>
> - **Flight (+2):** Winged kobolds fly at Pace 6.

## Kuo-toa

- **Aquatic (+2):** Kuo-toa can breathe underwater and move their full Pace when
  swimming.
- **Darkvision (+1):** Kuo-toa ignore penalties for Illumination up to 10" (20
  yards).
- **Otherworldly Perception (+1):** Kuo-toa can sense the presence of any
  creature within 6" (12 yards) that is invisible or ethereal. They ignore
  sight-based penalties for attacking such a creature, provided they are moving
  or have recently moved.
- **Slippery (+1):** Kuo-toa have a +2 bonus to rolls made to resist being
  grappled and rolls made to break free from being Bound or Entangled.
- **Sunlight Sensitivity (-2):** Kuo-toa are Distracted when exposed to
  sunlight.
- **Toughness +1 (+1):** Kuo-toa have a scaly skin, increasing their Toughness
  by 1.

## Lizardfolk

<!-- Fairly simple. -->

- **Armor +2 (+1):** Lizardfolk have tough, scaly skin, granting them Armor +2.
- **Bite (+2):** A lizardfolk's fanged maw is a **Natural Weapon** (see _Savage
  Worlds_) that causes Strength+d6 damage.
- **Environmental Weakness (Cold) (-1):** Lizardfolk suffer a -4 penalty to
  resist the cold, and suffer +4 damage from cold-based attacks.
- **Semi-Aquatic (+1):** Lizardfolk can hold their breath for 15 minutes. They
  move their full speed when swimming.

## Locathah

- **Aquatic (+2):** Locathah can breathe underwater and move their full Pace
  when swimming.
- **Leviathan Will (+3):** Locathah receive a +2 bonus to resist poison, being
  Stunned, being put to sleep, and magical effects that alter their mood or
  feelings.
- **Toughness +1 (+1):** Locathah have a scaly skin, increasing their Toughness
  by 1.
- **Water Dependency (-2):** Locathah must immerse themselves in water one hour
  out of every 24 or become automatically Fatigued each day until they are
  Incapacitated. The day after Incapacitation from dehydration, they perish.
  Each hour spent in water restores one level of Fatigue.

## Merfolk

- **Creature of the Sea (+2):** Merfolk can breathe underwater. Their Pace is 8,
  and they move at their full Pace when swimming. Each inch moved on land costs
  3" of Pace, and they suffer a -2 penalty to Athletics rolls related to
  movement on land.
- **Low Light Vision (+1):** Merfolk ignore penalties for Dim and Dark
  Illumination.
- **Skilled (+1):** Merfolk start with a d4 in any skill based on their
  experiences, education, training, or culture. If the chosen skill is a core
  skill, it starts at d6 and the skill's maximum increases to d12+1.

## Merrow

<!-- TODO: Technically Bite/Claws should be +1 each -->

- **Big (-2):** Merrow subtract two when using equipment designed for smaller
  beings, and cannot wear humanoid armor or clothing. Equipment, armor, food and
  clothing cost double the listed price.
- **Bite/Claws (+1):** A merrow's fangs and claws are **Natural Weapons** (see
  _Savage Worlds_) that cause Strength+d4 damage.
- **Creature of the Sea (+2):** Merrow can breathe underwater. Their Pace is 8,
  and they move at their full Pace when swimming. Each inch moved on land costs
  3" of Pace, and they suffer a -2 penalty to Athletics rolls related to
  movement on land.
- **Low Light Vision (+1):** Merrow ignore penalties for Dim and Dark
  Illumination.
- **Size +2 (+2):** Merrow are large sea creatures, increasing their Size (and
  therefore Toughness) by 2.

## Orc

<!-- Tall, strong, and darkvision. -->

- **Darkvision (+1):** Orcs can see in Pitch Darkness up to 10" (20 yards),
  ignoring all Illumination penalties and up to 2 points of penalties from
  invisibility or similar powers.
- **Powerful Build (+2):** Orcs start with a d6 in Strength instead of a d4.
  This increases maximum Strength to d12 + 1.
- **Size +1 (+1):** Orcs are hulking creatures, increasing their Size (and
  therefore Toughness) by 1.

## Sahuagin

- **Aquatic (+2):** Sahuagin can breathe underwater and move their full Pace
  when swimming.
- **Bite/Claws (+1):** A sahuagin's fangs and claws are **Natural Weapons** (see
  _Savage Worlds_) that cause Strength+d4 damage.
- **Blood Frenzy (+1):** When a sahuagin makes a Wild Attack against a bleeding
  creature, they add +4 to their damage instead of +2.
- **Darkvision (+1):** Sahuagin can see in Pitch Darkness up to 10" (20 yards),
  ignoring all Illumination penalties and up to 2 points of penalties from
  invisibility or similar powers.
- **Toughness +1 (+1):** Sahuagin have a scaly skin, increasing their Toughness
  by 1.
- **Water Dependency (-2):** Sahuagin must immerse themselves in water one hour
  out of every 24 or become automatically Fatigued each day until they are
  Incapacitated. The day after Incapacitation from dehydration, they perish.
  Each hour spent in water restores one level of Fatigue.

## Sea Elf

<!-- Almost identical to elves. Added Aquatic, removed Agile. -->

- **Aquatic (+2):** Sea elves can breathe underwater and move their full Pace
  when swimming.
- **Frail (-1):** Sea elves aren't as sturdy as most ancestries. Their Toughness
  is reduced by 1.
- **Keen Vision (+1):** Sea elves have a +2 to Notice rolls to detect obscured
  or hidden features.
- **Low Light Vision (+1):** Sea elves ignore penalties for Dim and Dark
  Illumination.
- **Trance (+1):** Sea elves don't need to sleep, but they must meditate in a
  semiconscious state 4 hours every day.

## Tiefling

- **Darkvision (+1):** Tieflings can see in Pitch Darkness up to 10" (20 yards),
  ignoring all Illumination penalties and up to 2 points of penalties from
  invisibility or similar powers.
- **Devil's Tongue (+2):** Tieflings start with a d6 in Spirit instead of a d4.
  This increases maximum Spirit to d12 + 1.
- **Hellish Resistance (+1):** Tieflings receive a +4 bonus to resist fire-based
  effects (including heat). Damage from these sources is reduced by 4.

> #### Variant: Winged Tieflings
>
> Replace Devil's Tongue with the following feature:
>
> - **Flight (+2):** Winged tieflings fly at Pace 6.

## Tortle

- **Claws (+1):** A tortle's claws are **Natural Weapons** (see _Savage Worlds_)
  that cause Strength+d4 damage.
- **Hard Shell (+1):** Tortles have a hard shell, granting them Armor +4.
  Tortles are unable to wear additional armor.
- **Semi-Aquatic (+1):** Tortles can hold their breath for 15 minutes.
- **Shell Defense (+1):** As a limited free action, a tortle can withdraw into
  or emerge from their shell. While withdrawn, the tortle can't take actions or
  move, attacks against them suffer a -2 penalty, and Called Shots are
  impossible.

## Triton

- **Aquatic (+2):** Tritons can breathe underwater and move their full Pace when
  swimming.
- **Darkvision (+1):** Tritons can see in Pitch Darkness up to 10" (20 yards),
  ignoring all Illumination penalties and up to 2 points of penalties from
  invisibility or similar powers.
- **Emissary of the Sea (+2):** Tritons have the Arcane Background (Gifted)
  Edge, and know the _summon animal_ power. The power has a Limitation such that
  it can only be used to summon marine animals.
